<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;

class Partner extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    public function city() {
        return $this->belongsTo('App\City');
    }

    public function hotels() {
        return $this->hasMany('App\Hotel');
    }
}
