<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;

class Admin extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    public $timestamps = false;
    protected $fillable =  ['Email', 'Password', 'Name'];

    public function city() {
        return $this->belongsTo('App\City');
    }
}
