<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    public function partner() {
        return $this->belongsTo('App\Partner');
    }

    public function city() {
        return $this->belongsTo('App\City', 'Location_id', 'Id');
    }

    public function photos() {
        return $this->hasMany('App\Photo', 'Hotel_id', 'Id');
    }
}
