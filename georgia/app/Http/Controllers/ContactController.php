<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/3/2016
 * Time: 6:44 PM
 */

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;


class ContactController extends Controller
{
    public function newContact(Request $request)
    {
        $this->validate($request,
            [
                'contact' => 'required|max:32',
                'email' => 'max:32',
                'subject' => 'required|max:512',
            ]);
        $contact = new Contact();
        $contact->Name = $request['contact'];
        $contact->Email = $request['email'];
        $contact->Subject = $request['subject'];
        $message = 'Сообщение не прошло валидацию';

        if ($contact->save())
            $message = 'Ваше сообщение успешно отправлено администрации';
        return redirect()->route('index')->with(['message' => $message]);
    }
}