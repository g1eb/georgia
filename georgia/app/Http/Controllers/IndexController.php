<?php

namespace App\Http\Controllers;

use App\Hotel;
use DB;

use App\Http\Requests;

class IndexController extends Controller
{
    public function index()
    {
        $mostRatingHotels = Hotel::orderBy('Rating', 'DESC')->take(10)->get();
        $mostRatingOwnerHotels = DB::table('hotels')
                                    ->join('partners', 'partners.Id', '=', 'hotels.Owner_Id')
                                    ->select('hotels.*')
                                    ->orderBy('partners.Rating', 'DESC')
                                    ->take(10)
                                    ->get();
        $mostRatingOwnerHotels = Hotel::hydrate($mostRatingOwnerHotels);
        /*echo "<pre>";
        print_r($mostRatingHotels);
        echo "</pre>";*/
        return view('index', [
            'mostRatingHotels' => $mostRatingHotels,
            'mostRatingOwnerHotels' => $mostRatingOwnerHotels
            ]);
    }

    public function news()
    {
        return view('news');
    }

    public function hotels()
    {
        return view('hotels');
    }

    public function signup()
    {
        return view('signup');
    }

    public function hotel($id)
    {
        $hotel = Hotel::find($id);
        return view ('hotel', ['hotel' => $hotel]);
    }
}
