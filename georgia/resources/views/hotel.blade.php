@extends('layouts.app')

<link rel="stylesheet" href="{{ URL::to('src/css/index.css') }}">

@section('content')

    <div class="row"></div>
    <div class="top-offset">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10 hotel-name">
                {{ $hotel["Name"] }}
            </div>
        </div>
    </div>

    <script src="{{ URL::to('src/js/jquery-2.2.4.min.js') }}" ></script>
    <script type="text/javascript">
        $('ul.menu-items').find('li.active-item').removeClass('active-item');
    </script>

@endsection