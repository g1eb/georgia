@extends('layouts.app')

@section('content')

    <div class="row"></div>
    <div class="top-offset">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <h1>Sign up</h1>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('ul.menu-items').find('li.active-item').removeClass('active-item');
        $('ul.menu-items').find('li.menu-item.dropdown').addClass('active-item');
    </script>

@endsection