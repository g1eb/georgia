<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::to('src/css/common.css') }}">
    <link rel="stylesheet" href="{{ URL::to('src/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('src/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('src/css/smoothTouchScroll.css') }}">
    <script src="{{ URL::to('src/js/jquery-2.2.4.min.js') }}" ></script>
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
</head>
<body id="app-layout">

    <header>
        <div class="top-menu pulse wow" data-wow-delay="0.5s">
            <ul class="menu-items">
                <li class="brand">Welcome to Georgia</li>
                <li class="menu-item active-item"><a href="{{ url('/') }}">Главная</a></li>
                <li class="menu-item"><a href="{{ url('news') }}">Новости</a></li>
                <li class="menu-item"><a href="{{ url('hotels') }}">Отели</a></li>
                <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Кабинет <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        @if(Auth::guest())
                            <li class="dropdown-menu-new-item"><a href="{{ url('login') }}"><i class="fa fa-btn fa-sign-out"></i> Войти</a></li>
                            <li class="dropdown-menu-new-item"><a href="{{ url('signup') }}"><i class="fa fa-btn fa-sign-out"></i> Создать аккаунт</a></li>
                        @else
                            <li class="dropdown-menu-new-item"><a href="{{ url('logout') }}"><i class="fa fa-btn fa-sign-out"></i> Выйти</a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
    </header>

    @yield('content')

    <footer>
            <p>Copyright © Welcome to Georgia 2016</p>
    </footer>

    <!-- JavaScripts -->
    <script src="{{ URL::to('src/js/jquery-2.2.4.min.js') }}" ></script>
    <script src="{{ URL::to('src/js/jquery-ui-1.10.3.custom.min.js') }}" ></script>
    <script src="{{ URL::to('src/js/jquery.kinetic.min.js') }}" ></script>
    <script src="{{ URL::to('src/js/jquery.mousewheel.min.js') }}" ></script>
    <script src="{{ URL::to('src/js/jquery.smoothdivscroll-1.3-min.js') }}" ></script>
    <script src="{{ URL::to('src/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('src/js/wow.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#messageBox").modal('show');
        });
    </script>
    <script>
        $(document).ready(function() {
            $(".mixedContent").smoothDivScroll({
                hotSpotScrolling: false,
                touchScrolling: true
            });
        });
    </script>
    <script>
        new WOW().init();
    </script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
