@extends('layouts.app')

@section('content')

    <div class="row"></div>
    <div class="top-offset">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <h1>News</h1>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('ul.menu-items').find('li.active-item').removeClass('active-item');
        $('ul.menu-items').find('li:nth-child(3)').addClass('active-item');
    </script>

@endsection