@extends('layouts.app')

<link rel="stylesheet" href="{{ URL::to('src/css/index.css') }}">

<link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=EB+Garamond&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<!--
а б в г д е ё ж з и й к л м н
 -->

@section('content')
    @include('includes.messages')
        <div class="index-container slideInRight wow" data-wow-delay="1s">
            <div id="indexCarousel" class="carousel slide" data-ride="carousel" data-interval="3000">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#indexCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#indexCarousel" data-slide-to="1"></li>
                    <li data-target="#indexCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="{{ URL::to('src/img/slides/slide2.jpg') }}" alt="">
                        <div class="carousel-caption">
                            <h3>Chania</h3>
                            <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="{{ URL::to('src/img/slides/slide5.jpg') }}" alt="">
                        <div class="carousel-caption">
                            <h3>Chania</h3>
                            <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="{{ URL::to('src/img/slides/slide6.jpg') }}" alt="">
                        <div class="carousel-caption">
                            <h3>Chania</h3>
                            <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="index-container">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <h1 class="caption fadeInDown wow" data-wow-delay="2s">Кто мы и что предлагаем</h1>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="fadeInDown wow" data-wow-delay="2.5s">
                    <div class="col-xs-2">
                        <i class="icon-desc glyphicon glyphicon-education"></i>
                    </div>
                    <div class="col-xs-4">
                        <p class="description">{{ Consts::PROJECT_NAME }} is a Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet, beatae culpa cum dolores earum error ex facere maiores modi nostrum officia officiis quae quasi quod reiciendis rem unde voluptates.</p>
                    </div>
                </div>
                <div class="fadeInDown wow" data-wow-delay="3s">
                    <div class="col-xs-1">
                        <i class="icon-desc glyphicon glyphicon-trash"></i>
                    </div>
                    <div class="col-xs-4">
                        <p class="description">{{ Consts::PROJECT_NAME }} is a Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet, beatae culpa cum dolores earum error ex facere maiores modi nostrum officia officiis quae quasi quod reiciendis rem unde voluptates.</p>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="fadeInDown wow" data-wow-delay="3.5s">
                    <div class="col-xs-2">
                        <i class="icon-desc glyphicon glyphicon-map-marker"></i>
                    </div>
                    <div class="col-xs-4">
                        <p class="description">{{ Consts::PROJECT_NAME }} is a Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet, beatae culpa cum dolores earum error ex facere maiores modi nostrum officia officiis quae quasi quod reiciendis rem unde voluptates.</p>
                    </div>
                </div>
                <div class="fadeInDown wow" data-wow-delay="4s">
                    <div class="col-xs-1">
                        <i class="icon-desc glyphicon glyphicon-calendar"></i>
                    </div>
                    <div class="col-xs-4">
                        <p class="description">{{ Consts::PROJECT_NAME }} is a Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet, beatae culpa cum dolores earum error ex facere maiores modi nostrum officia officiis quae quasi quod reiciendis rem unde voluptates.</p>
                    </div>
                </div>
            </div>
            <br/>
            <div class="bg-gray">
                <div class="col-xs-10 col-xs-offset-1">
                    <h1 class="caption fadeInDown wow" data-wow-delay="0.5s">Лучшие отели по мнению гостей</h1>
                    <div class="mixedContent fadeInRight wow" data-wow-delay="1s">
                        <?php $count = count($mostRatingHotels); ?>
                        @for($i = 0; $i < $count; $i++)
                            <ul class="contentBox">
                                <li class="hotel-name">{{ $mostRatingHotels[$i]["Name"] }}</li>
                                <?php
                                    $url = 'src/img/hotels/nopic.png';
                                    $photos = $mostRatingHotels[$i]->photos;
                                    if (count($photos) > 0) $url = $photos[0]->toArray()["Photo"];
                                ?>
                                <li class="hotel-image"><img src="{{ URL::to($url) }}" alt="Hotel profile image" ></li>
                                <li class="hotel-stars">
                                    <?php
                                        $stars = $mostRatingHotels[$i]["Stars"];
                                        if (!$stars) echo "<i class=\"glyphicon glyphicon-star-empty\"></i>";
                                        else {
                                            $j = 0;
                                            for(; $j < $stars; $j++)
                                                echo "<i class=\"glyphicon glyphicon-star active\"></i>";
                                            for(; $j < 5; $j++)
                                                echo "<i class=\"glyphicon glyphicon-star\"></i>";
                                        }
                                    ?>
                                </li>
                                <li class="hotel-location">{{ $mostRatingHotels[$i]->city["Name"] }}</li>
                                <li class="hotel-status">
                                    <?php
                                    if($mostRatingHotels[$i]["Rating"] < 0) echo 'Отель с плохой репутацией';
                                    else if ($mostRatingHotels[$i]["Rating"] >= 0 && $mostRatingHotels[$i]["Rating"] < 10) echo 'Недавно зарегистрированный отель';
                                    else if ($mostRatingHotels[$i]["Rating"] >= 10 && $mostRatingHotels[$i]["Rating"] < 50) echo 'Хороший отель';
                                    else if ($mostRatingHotels[$i]["Rating"] >= 50 && $mostRatingHotels[$i]["Rating"] < 80) echo 'Отличный отель';
                                    else if ($mostRatingHotels[$i]["Rating"] >= 80) echo 'Отель из класса наилучших';
                                    else echo 'Оценка недоступна';
                                    ?>
                                </li>
                                <li class="hotel-price">Номера от ${{ $mostRatingHotels[$i]["Price"] }}</li>
                                <li class="hotel-details"><a href="{{ url('hotel/' . $mostRatingHotels[$i]["Id"]) }}">Подробный обзор</a></li>
                            </ul>
                        @endfor
                    </div>
                </div>
            </div>
            <div class="bg-gray">
                <div class="col-xs-10 col-xs-offset-1">
                    <h1 class="caption fadeInDown wow" data-wow-delay="0.5s">Отели с наивысшим рейтингом владельцев</h1>
                    <div class="mixedContent fadeInRight wow" data-wow-delay="1s">
                        <?php $count = count($mostRatingOwnerHotels); ?>
                        @for($i = 0; $i < $count; $i++)
                            <ul class="contentBox">
                                <li class="hotel-name">{{ $mostRatingOwnerHotels[$i]["Name"] }}</li>
                                <?php
                                $url = 'src/img/hotels/nopic.png';
                                $photos = $mostRatingOwnerHotels[$i]->photos;
                                if (count($photos) > 0) $url = $photos[0]->toArray()["Photo"];
                                ?>
                                <li class="hotel-image"><img src="{{ URL::to($url) }}" alt="Hotel profile image" ></li>
                                <li class="hotel-stars">
                                    <?php
                                    $stars = $mostRatingOwnerHotels[$i]["Stars"];
                                    if (!$stars) echo "<i class=\"glyphicon glyphicon-star-empty\"></i>";
                                    else {
                                        $j = 0;
                                        for(; $j < $stars; $j++)
                                            echo "<i class=\"glyphicon glyphicon-star active\"></i>";
                                        for(; $j < 5; $j++)
                                            echo "<i class=\"glyphicon glyphicon-star\"></i>";
                                    }
                                    ?>
                                </li>
                                <li class="hotel-location">{{ $mostRatingOwnerHotels[$i]->city["Name"] }}</li>
                                <li class="hotel-status">
                                    <?php
                                    if($mostRatingOwnerHotels[$i]["Rating"] < 0) echo 'Отель с плохой репутацией';
                                    else if ($mostRatingOwnerHotels[$i]["Rating"] >= 0 && $mostRatingOwnerHotels[$i]["Rating"] < 10) echo 'Недавно зарегистрированный отель';
                                    else if ($mostRatingOwnerHotels[$i]["Rating"] >= 10 && $mostRatingOwnerHotels[$i]["Rating"] < 50) echo 'Хороший отель';
                                    else if ($mostRatingOwnerHotels[$i]["Rating"] >= 50 && $mostRatingOwnerHotels[$i]["Rating"] < 80) echo 'Отличный отель';
                                    else if ($mostRatingOwnerHotels[$i]["Rating"] >= 80) echo 'Отель из класса наилучших';
                                    else echo 'Оценка недоступна';
                                    ?>
                                </li>
                                <li class="hotel-price">Номера от ${{ $mostRatingOwnerHotels[$i]["Price"] }}</li>
                                <li class="hotel-details"><a href="{{ url('hotel/' . $mostRatingOwnerHotels[$i]["Id"]) }}">Подробный обзор</a></li>
                            </ul>
                        @endfor
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <h1 class="caption fadeInDown wow" data-wow-delay="0.5s">Есть что обсудить? Напишите нам</h1>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-xs-6 col-xs-offset-1 fadeInLeftBig wow" data-wow-delay="1s">
                    <div id="map"></div>
                    <script>
                        function initMap() {
                            var mapDiv = document.getElementById('map');
                            var map = new google.maps.Map(mapDiv, {
                                center: {lat: 40.7128, lng: -74.0059},
                                mapTypeId:google.maps.MapTypeId.ROADMAP,
                                zoom: 10
                            });
                        }
                    </script>
                    <script async defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJPjXWuNC52-2sF62LRlc7erj0pqm6kRU&callback=initMap">
                    </script>
                </div>
                <ul class="contact-form col-xs-4 fadeInRightBig wow" data-wow-delay="1.5s">
                    <li class="caption">Форма связи</li>
                    <li class="content">
                        <form action="{{ route('new.contact') }}" method="GET">
                            <div class="form-group inner-addon right-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                                <label for="email">Адрес вашей электронной почты</label>
                                <input name="email" id="email" class="form-control" type="email">
                            </div>
                            <div class="form-group inner-addon right-addon">
                                <i class="glyphicon glyphicon-user"></i>
                                <label for="contact">Ваше имя</label>
                                <input name="contact" id="contact" class="form-control" type="text">
                            </div>
                            <div class="form-group">
                                <label for="subject">Сообщение</label>
                                <textarea name="subject" id="subject" rows="8" class="form-control"></textarea>
                            </div>
                            <div class="contact-btn">
                                <button type="submit" class="hotel-details">Отправить</button>
                                <input type="hidden" value="{{ Session::token() }}" name="_token">
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
        <br/><br/><br/><br/><br/>

    <script type="text/javascript">
        $('ul.menu-items').find('li.active-item').removeClass('active-item');
        $('ul.menu-items').find('li:nth-child(2):first').addClass('active-item');
    </script>
@endsection