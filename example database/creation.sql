CREATE DATABASE IF NOT EXISTS `Georgination`;

/*DROP DATABASE `Georgination`;*/
USE `Georgination`;

CREATE TABLE IF NOT EXISTS `Countries`
(
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
    `Name` VARCHAR(32) NOT NULL,
    
    PRIMARY KEY (`Id`)
);

CREATE TABLE IF NOT EXISTS `Cities`
(
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
    `Name` VARCHAR(32) NOT NULL,
    `Country_id` INT NOT NULL,
    
    PRIMARY KEY (`Id`),
    FOREIGN KEY (`Country_id`) REFERENCES `Countries`(Id) 
);

CREATE TABLE IF NOT EXISTS `Admins`
(
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
    `Name` VARCHAR(64) NOT NULL,
    `Email` VARCHAR(32) NOT NULL UNIQUE,
    `Password` VARCHAR(33) NOT NULL,
    `Key` VARCHAR(16) NOT NULL,
    `Birthday` DATE NOT NULL,
    `Photo` VARCHAR(256) DEFAULT 'src/img/nopic.jpg',
    `Level` INT DEFAULT 0,
    `Location_id` INT NOT NULL,
    
    PRIMARY KEY (`Id`),
    FOREIGN KEY (`Location_id`) REFERENCES `Cities`(Id)
);

CREATE TABLE IF NOT EXISTS `Users`
(
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
    `Email` VARCHAR(32) NOT NULL UNIQUE,
    `Password` VARCHAR(33) NOT NULL,
    `Name` VARCHAR(64) NOT NULL,
    `Remember_token` BOOLEAN,
    `Birthday` DATE NOT NULL,
    `Photo` VARCHAR(256) DEFAULT 'src/img/nopic.jpg',
    `Rating` INT DEFAULT 0,		/* Очки. Даются за поездки */
    `AllowInvites` BOOLEAN DEFAULT 1,	/* могут ли бизнесмены приглашать */
    `Skype` VARCHAR(16),
    `VK` VARCHAR(16),
    `Facebook` VARCHAR(16),
    `Phone` VARCHAR(32) NOT NULL,
    `Location_id` INT NOT NULL,
    
    PRIMARY KEY (`Id`),
    FOREIGN KEY (`Location_id`) REFERENCES `Cities`(Id)
);

CREATE TABLE IF NOT EXISTS `Partners`
(
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
    `Email` VARCHAR(32) NOT NULL UNIQUE,
    `Password` VARCHAR(33) NOT NULL,
    `Key` VARCHAR(16) NOT NULL,
    `Name` VARCHAR(64) NOT NULL,
    `Remember_token` BOOLEAN,
    `Birthday` DATE NOT NULL,
    `Photo` VARCHAR(256) DEFAULT 'src/img/nopic.jpg',
    `Rating` INT DEFAULT 0,		/* Очки. Даются за отзывы */
    `Autobiography` VARCHAR(1024),
    `Skype` VARCHAR(16),
    `VK` VARCHAR(16),
    `Facebook` VARCHAR(16),
    `Phone` VARCHAR(32) NOT NULL,
    `Location_id` INT NOT NULL,
    `Citizenship_id` INT NOT NULL,
    
    PRIMARY KEY (`Id`),
    FOREIGN KEY (`Location_id`) REFERENCES `Cities`(Id),
    FOREIGN KEY (`Citizenship_id`) REFERENCES `Countries`(Id)
);

CREATE TABLE IF NOT EXISTS `Hotels`
(
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
    `Name` VARCHAR(32) NOT NULL,
    `Street` VARCHAR(64) NOT NULL,
    `Location_id` INT NOT NULL,
    `Owner_id` INT NOT NULL,
    `Rating` INT DEFAULT 0,
    `Stars` TINYINT CHECK (`Stars` >= 0 AND `Stars` <= 5),
    `Price` INT NOT NULL,
    `Wifi` BOOLEAN NOT NULL,
    `Breakfast` BOOLEAN NOT NULL,
    `Parking` BOOLEAN NOT NULL,
    `Restaurant` BOOLEAN NOT NULL,
    `Transfer` BOOLEAN NOT NULL,
    `Phone` VARCHAR(32) NOT NULL,
    `Email` VARCHAR(32),
    
    PRIMARY KEY (`Id`),
    FOREIGN KEY (`Owner_id`) REFERENCES `Partners`(Id),
    FOREIGN KEY (`Location_id`) REFERENCES `Cities`(Id)
);

CREATE TABLE IF NOT EXISTS `RoomTypes`
(
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
    `Type` VARCHAR(32)
);

CREATE TABLE IF NOT EXISTS `Rooms`
(
	`Hotel_id` INT NOT NULL,
    `RoomType_id` INT NOT NULL,
   
    FOREIGN KEY (`Hotel_id`) REFERENCES `Hotels`(Id),
    FOREIGN KEY (`RoomType_id`) REFERENCES `RoomTypes`(Id)
);

/*
`Price` DECIMAL NOT NULL, /* за ночь 
`Wifi` BOOLEAN NOT NULL,
    `TV` BOOLEAN NOT NULL,
    `Conditioner` BOOLEAN NOT NULL,
    `Bathroom` BOOLEAN NOT NULL, /* ванная в номере
*/

CREATE TABLE IF NOT EXISTS `Photos`
(
	`Hotel_id` INT NOT NULL,
    `Photo` VARCHAR(256),
    
    FOREIGN KEY (`Hotel_id`) REFERENCES `Hotels`(Id)
);

CREATE TABLE IF NOT EXISTS `ReviewTypes`
(
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
    `Type` VARCHAR(32)
);

CREATE TABLE IF NOT EXISTS `Reviews`
(
	`Hotel_id` INT NOT NULL,
    `User_id` INT NOT NULL,
    `ReviewType_id` INT NOT NULL,
    `Text` VARCHAR(512),
    
    FOREIGN KEY (`Hotel_id`) REFERENCES `Hotels`(Id),
    FOREIGN KEY (`User_id`) REFERENCES `Users`(Id),
    FOREIGN KEY (`ReviewType_id`) REFERENCES `ReviewTypes`(Id)
);

CREATE TABLE IF NOT EXISTS `Contacts`
(
	`Id` INT NOT NULL AUTO_INCREMENT UNIQUE,
    `Name` VARCHAR(32) NOT NULL,
    `Email` VARCHAR(32) NOT NULL,
    `Subject` VARCHAR(512) NOT NULL
);




















